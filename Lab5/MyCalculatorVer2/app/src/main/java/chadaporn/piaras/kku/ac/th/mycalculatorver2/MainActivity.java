package chadaporn.piaras.kku.ac.th.mycalculatorver2;


import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.Toast;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity{

    float numL, numR;
    float sum = 0;

    EditText editTextL;
    EditText editTextR;
    TextView showText;
    RadioGroup rbGroup;
    Switch sw;
    Button btn;
    Long currentTime, stopTime;
    float calTime;
    boolean checkInput = false;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_setting) {
            Toast.makeText(MainActivity.this,
                    "choose action settings", Toast.LENGTH_LONG).show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextL = (EditText) findViewById(R.id.edit_text_L);
        editTextR = (EditText) findViewById(R.id.edit_text_R);
        showText = (TextView) findViewById(R.id.show_text);
        rbGroup = (RadioGroup) findViewById(R.id.rbg);
        sw = (Switch) findViewById(R.id.switch_);
        btn = (Button) findViewById(R.id.calButton);

        String msgSize="";

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        msgSize = "Width = " + width + ", " + "Height = " +  height;

        Toast.makeText(MainActivity.this, msgSize, Toast.LENGTH_LONG).show();


        rbGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                calculate(checkedId);
            }
        });

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view == btn) {
                    calculate(rbGroup.getCheckedRadioButtonId());
                }
            }
        });


        sw.setText("OFF");
        sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (sw.isChecked()) {
                    sw.setText("ON");
                }
                else sw.setText("OFF");
            }
        });

    }

    private void acceptNumbers() {
        try {
            currentTime = System.currentTimeMillis();
            numL = Float.parseFloat(editTextL.getText().toString());
            numR = Float.parseFloat(editTextR.getText().toString());
            checkInput = true;
        } catch (Exception e) {
            showToast("Please enter only number");
            showText.setText(" = ");
        }
    }


    private void showToast(String msg) {
        Toast.makeText(MainActivity.this, msg, Toast.LENGTH_LONG).show();
    }

    private void calculate(int id) {

        acceptNumbers();
        if (checkInput) {
            switch (id) {
                case R.id.rb_add:
                    sum = numL + numR;
                    break;
                case R.id.rb_sub:
                    sum = numL - numR;
                    break;
                case R.id.rb_mul:
                    sum = numL * numR;
                    break;
                case R.id.rb_div:
                    if (numR != 0)
                        sum = numL / numR;
                    else showToast("Please divide by a non-zero number");
                    break;
            }
        }
        stopTime = System.currentTimeMillis();
        calTime = (stopTime - currentTime)/1000.0f;
        Log.d("Calculation","computation time = " + calTime);
        showText.setText(" = "+sum);
        checkInput = false;
    }
}

package chadaporn.piaras.kku.ac.th.testfirebase;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.Manifest.permission.READ_CONTACTS;

public  class LoginActivity extends AppCompatActivity {

    DatabaseReference myRef;
    String TAG = "chompu";
    FirebaseDatabase database;
    EditText usernameEdittext, emailEdittext, usernameEdittext2, emailEdittext2, usernameEdittext3, emailEdittext3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();
        String email = bundle.getString("email");

        usernameEdittext = (EditText) findViewById(R.id.ed_name);
        emailEdittext = (EditText) findViewById(R.id.ed_email);
        usernameEdittext2 = (EditText) findViewById(R.id.ed_name2);
        emailEdittext2 = (EditText) findViewById(R.id.ed_email2);
        usernameEdittext3 = (EditText) findViewById(R.id.ed_name3);
        emailEdittext3 = (EditText) findViewById(R.id.ed_email3);

        TextView tvDetail = (TextView) findViewById(R.id.tv_detail);
        final CharSequence message = email + " is logged in successfully";
        tvDetail.setText(message);

        ((Button)findViewById(R.id.bt_log_out)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                onBackPressed();
            }
        });


        ((Button)findViewById(R.id.bt_update)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sUsername = usernameEdittext.getText().toString();
                String sEmail = emailEdittext.getText().toString();
                FirebaseDatabase db = FirebaseDatabase.getInstance();
                DatabaseReference dbRef = db.getReference("/message/users/");
                Map<String, Object> childUpdates = new HashMap<>();

                childUpdates.put(sUsername + "/email/", sEmail);

                dbRef.updateChildren(childUpdates);

            }
        });


        ((Button)findViewById(R.id.bt_adduser)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sUsername2 = usernameEdittext2.getText().toString();
                String sEmail2 = emailEdittext2.getText().toString();
                String sUsername3 = usernameEdittext3.getText().toString();
                String sEmail3 = emailEdittext3.getText().toString();

                FirebaseDatabase db = FirebaseDatabase.getInstance();
                DatabaseReference dbRef = db.getReference("message");
                dbRef.child("users").child("userId1").child("email").setValue(sEmail2);
                dbRef.child("users").child("userId1").child("username").setValue(sUsername2);
                dbRef.child("users").child("userId2").child("email").setValue(sEmail3);
                dbRef.child("users").child("userId2").child("username").setValue(sUsername3);

            }
        });

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("message");
//        writeDB();
//        readDB();
        writeNewUser("chompusama", "chompu", "chompu@gmail.com");
        readNewUer();

    }

    private void writeDB() {
        myRef.setValue("Hello, world");
    }

    private void readDB() {
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                Log.d(TAG, "value is : " + value);
                Toast.makeText(LoginActivity.this,
                        "Value is " + value,
                        Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.w(TAG, "Failed to read value.", databaseError.toException());
            }
        });
    }


    private void writeNewUser(String userId, String name, String email) {
        String key = myRef.child("users").push().getKey();
        User user = new User(name, email);
        Map<String, Object> userValues = user.toMap();

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/users/" + userId, userValues);
        myRef.updateChildren(childUpdates);
    }

    private void readNewUer() {
        myRef.child("users").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                User user = dataSnapshot.getValue(User.class);
                Toast.makeText(LoginActivity.this,
                        "Username is " + user.toMap().get("username"),
                        Toast.LENGTH_LONG).show();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                User user = dataSnapshot.getValue(User.class);
                Toast.makeText(LoginActivity.this,
                        "Email is " + user.toMap().get("email"),
                        Toast.LENGTH_LONG).show();
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


}
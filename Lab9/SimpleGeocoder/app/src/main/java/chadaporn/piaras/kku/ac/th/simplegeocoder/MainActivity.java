package chadaporn.piaras.kku.ac.th.simplegeocoder;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    RadioGroup rdg;
    RadioButton latlng, addr;
    EditText lat, lng, address;
    Geocoder geocoder;
    Button fetchButton;
    TextView showAddr;
    String result = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rdg = (RadioGroup) findViewById(R.id.radiogroup);
        latlng = (RadioButton) findViewById(R.id.latlng_b);
        addr = (RadioButton) findViewById(R.id.addr_b);
        lat = (EditText) findViewById(R.id.lat_edit);
        lng = (EditText) findViewById(R.id.lng_edit);
        address = (EditText) findViewById(R.id.addr_edit);
        fetchButton = (Button) findViewById(R.id.fetch_button);
        showAddr = (TextView) findViewById(R.id.show_textview);

        rdg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.latlng_b:
                        lat.setEnabled(true);
                        lng.setEnabled(true);
                        address.setEnabled(false);
                        break;
                    case R.id.addr_b:
                        lat.setEnabled(false);
                        lng.setEnabled(false);
                        address.setEnabled(true);
                }
            }
        });

        fetchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                geocoder = new Geocoder(MainActivity.this, Locale.getDefault());
                result = null;
                if (latlng.isChecked()) {
                    try {
                        List<Address> list = geocoder.getFromLocation(
                                Double.parseDouble(lat.getText().toString())
                                , Double.parseDouble(lng.getText().toString())
                                , 1);
                        Address address = list.get(0);
                        result = address.getAddressLine(0) + ", " + address.getLocality();
                        showAddr.setText(result);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        List<Address> list = geocoder.getFromLocationName(
                                address.getText().toString(), 1);
                        Address address = list.get(0);
                        result = "Lat: = " + address.getLatitude() + "\n" +
                                "Lng: = " + address.getLongitude();
                        showAddr.setText(result);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }


            }
        });


    }


}

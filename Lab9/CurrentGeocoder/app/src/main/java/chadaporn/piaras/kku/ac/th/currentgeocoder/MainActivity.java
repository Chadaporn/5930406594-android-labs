package chadaporn.piaras.kku.ac.th.currentgeocoder;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import android.os.Handler;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    RadioGroup rdg;
    RadioButton latlng, addr;
    EditText lat, lng, addressEdittext;
    Geocoder geocoder;
    Button fetchButton;
    TextView showAddr;
    String result = "";

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    final Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rdg = (RadioGroup) findViewById(R.id.radiogroup);
        latlng = (RadioButton) findViewById(R.id.latlng_b);
        addr = (RadioButton) findViewById(R.id.addr_b);
        lat = (EditText) findViewById(R.id.lat_edit);
        lng = (EditText) findViewById(R.id.lng_edit);
        addressEdittext = (EditText) findViewById(R.id.addr_edit);
        fetchButton = (Button) findViewById(R.id.fetch_button);
        showAddr = (TextView) findViewById(R.id.show_textview);

        requestPermissions();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        rdg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // image of 4th choice in lab 9 sheet ENABLE ALL edittext
                switch (checkedId) {
                    case R.id.latlng_b:
                        lat.setEnabled(true);
                        lng.setEnabled(true);
                        addressEdittext.setEnabled(true);
                        break;
                    case R.id.addr_b:
                        lat.setEnabled(true);
                        lng.setEnabled(true);
                        addressEdittext.setEnabled(true);
                }
            }
        });

        fetchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                geocoder = new Geocoder(MainActivity.this, Locale.getDefault());
                result = null;

                //automatically display current latlng once
                mGoogleApiClient.disconnect();

                if (latlng.isChecked()) {
                    try {
                        List<Address> list = geocoder.getFromLocation(
                                Double.parseDouble(lat.getText().toString())
                                , Double.parseDouble(lng.getText().toString())
                                , 1);
                        Address address = list.get(0);
                        result = address.getAddressLine(0) + ", " + address.getLocality();
                        addressEdittext.setText(result);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                } else {
                    try {
                        List<Address> list = geocoder.getFromLocationName(
                                addressEdittext.getText().toString(), 1);
                        Address address = list.get(0);
                        lat.setText("" + address.getLatitude());
                        lng.setText("" + address.getLongitude());

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }


            }
        });


    }

    private void requestPermissions() {
        RxPermissions rxPermissions = new RxPermissions(this);
        rxPermissions
                .request(Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION)
                .subscribe(granted -> {
                    if (granted) {
                    } else {
                    }
                });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                mLocationRequest, this );
    }


    @Override
    public void onLocationChanged(Location location) {
        Toast.makeText(this, "onLocationChanged", Toast.LENGTH_LONG).show();
        lat.setText("" + location.getLatitude());
        lng.setText(""+ location.getLongitude());;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    @Override
    protected  void onPause() {
        super.onPause();
        if(mGoogleApiClient.isConnected())
            mGoogleApiClient.disconnect();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}

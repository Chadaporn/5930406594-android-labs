package chadaporn.piaras.kku.ac.th.wdpviewdemo;

import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    int numL, numR;
    int sum = 0;

    Button btn;
    EditText editTextL;
    EditText editTextR;
    TextView result;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn = (Button) findViewById(R.id.calButton);
        btn.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        editTextL = (EditText) findViewById(R.id.edit_text_L);
        editTextR = (EditText) findViewById(R.id.edit_text_R);
        result = (TextView) findViewById(R.id.show_text);

        numL = Integer.parseInt(editTextL.getText().toString());
        numR = Integer.parseInt(editTextR.getText().toString());

        sum = numL + numR;

        result.setText(" "+sum);
    }
}
